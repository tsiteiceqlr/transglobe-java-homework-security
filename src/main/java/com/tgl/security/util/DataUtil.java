package com.tgl.security.util;

public class DataUtil {

	private DataUtil() {}

	public static String maskChineseName(String chineseName) {
		if (chineseName == null || chineseName.length() == 0) {
			return "";
		}

		StringBuilder mask = new StringBuilder();
		mask.append(chineseName.substring(0, 1));
		mask.append("*");
		if (chineseName.length() == 3) {
			mask.append(chineseName.substring(2, 3));
		}
		if (chineseName.length() == 4) {
			mask.append("*");
			mask.append(chineseName.substring(3, 4));
		}
		return mask.toString();
	}

	public static float bmi(int height, int weight) {
		return ((float) weight / (float) Math.pow(((float) height / 100), 2));
	}

}
