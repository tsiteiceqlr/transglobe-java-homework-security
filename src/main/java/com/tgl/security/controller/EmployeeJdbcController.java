package com.tgl.security.controller;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tgl.security.exception.RecordNotFoundException;
import com.tgl.security.exception.ValueNotValidException;
import com.tgl.security.model.Employee;
import com.tgl.security.service.EmployeeJdbcService;
import com.tgl.security.util.DataUtil;

@RestController
@RequestMapping(value = "/rest/employee/jdbc")
public class EmployeeJdbcController {

	private static final Logger logger = LogManager.getLogger(EmployeeJdbcController.class);

	@Autowired
	private EmployeeJdbcService employeeService;

	@PostMapping
	public ResponseEntity<Integer> insert(@RequestBody @Valid Employee employee) {
		int createdId = employeeService.insert(employee);
		return new ResponseEntity<>(createdId, HttpStatus.CREATED);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Integer> delete(@PathVariable("id") @Min(1) int employeeId) {
		Employee result = employeeService.findById(employeeId);
		if (result == null) {
			throw new RecordNotFoundException(employeeId);
		}
		employeeService.delete(employeeId);
		return new ResponseEntity<>(employeeId, HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<Employee> update(@RequestBody @Valid Employee employee) {
		Employee result = employeeService.findById(employee.getId());
		if (result == null) {
			throw new RecordNotFoundException(employee.getId());
		}
		employeeService.update(employee);
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<Employee> findById(@PathVariable("id") @Min(1) int employeeId) {
		Employee employee = employeeService.findById(employeeId);
		if (employee == null) {
			throw new RecordNotFoundException(employeeId);
		}
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}

	@PostMapping("/batchInsert")
	public ResponseEntity<Integer> batchInsert(@RequestParam("file") MultipartFile file) {
		List<Employee> employeeList = new ArrayList<>();
		if (!file.isEmpty()) {
			try (InputStream inputStream = new BufferedInputStream(file.getInputStream());
					BufferedReader read = new BufferedReader(
							new InputStreamReader(inputStream, StandardCharsets.UTF_8));) {
				String line = null;
				while ((line = read.readLine()) != null) {
					String[] data = line.split(",");
					if (data == null || data.length != 6) {
						continue;
					}
					Employee employee = new Employee();
					if (Integer.parseInt(data[0]) > 250 || Integer.parseInt(data[0]) < 100) {
						throw new ValueNotValidException("height: " + data[0]);
					}
					if (Integer.parseInt(data[1]) > 200 || Integer.parseInt(data[1]) < 30) {
						throw new ValueNotValidException("weight: " + data[1]);
					}
					if (!data[2].matches("[a-zA-Z]+-[a-zA-Z]+")) {
						throw new ValueNotValidException("english name: " + data[2]);
					}
					if (!data[3].matches("[\\u4E00-\\u9FA5]{2,3}")) {
						throw new ValueNotValidException("chinese name: " + data[3]);
					}
					if (!data[4].matches("[0-9]{4}")) {
						throw new ValueNotValidException("phone: " + data[4]);
					}
					if (!data[5].matches("[a-zA-Z0-9._%+]+@[a-zA-Z0-9.-]+\\.[a-z]{2,6}")) {
						throw new ValueNotValidException("email: " + data[5]);
					}
					employee.setHeight(Integer.parseInt(data[0]));
					employee.setWeight(Integer.parseInt(data[1]));
					employee.setEnglishName(data[2]);
					employee.setChineseName(data[3]);
					employee.setPhone(data[4]);
					employee.setEmail(data[5]);
					employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight()));
					employeeList.add(employee);
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
		logger.info("success convert EmployeeData");
		employeeService.batchInsert(employeeList);
		return new ResponseEntity<>(employeeList.size(), HttpStatus.CREATED);
	}

}
