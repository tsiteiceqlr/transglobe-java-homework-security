package com.tgl.security.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tgl.security.model.Employee;

@Repository
@Qualifier("employeeMyBatisDao")
public interface EmployeeMyBatisDao {
	
	int insert(Employee employee);
	
	boolean delete(int id);
	
	boolean update(Employee employee);
	
	Employee findById(int id);
	
	@Transactional(rollbackFor = SQLException.class)
	boolean batchInsert(List<Employee> employeeList);

}
