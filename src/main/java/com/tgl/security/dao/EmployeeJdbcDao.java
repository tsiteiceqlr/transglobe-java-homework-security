package com.tgl.security.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.tgl.security.mapper.EmployeeRowMapper;
import com.tgl.security.model.Employee;

@Repository
@Qualifier("employeeJdbcDao")
public class EmployeeJdbcDao {
	
	private static final Logger logger = LogManager.getLogger(EmployeeJdbcDao.class);
	
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private static final String INSERT = "INSERT INTO employee (HEIGHT, WEIGHT, ENGLISH_NAME, CHINESE_NAME, PHONE, EMAIL, BMI) VALUES (:height, :weight, :englishName, :chineseName, :phone, :email, :bmi)";

	private static final String DELETE = "DELETE FROM employee WHERE ID=:id";

	private static final String UPDATE = "UPDATE employee SET HEIGHT=:height, WEIGHT=:weight, ENGLISH_NAME=:englishName, CHINESE_NAME=:chineseName, PHONE=:phone, EMAIL=:email, BMI=:bmi WHERE ID=:id";

	private static final String SELECT = "SELECT ID, HEIGHT, WEIGHT, ENGLISH_NAME, CHINESE_NAME, PHONE, EMAIL, BMI FROM employee WHERE ID=:id";

	public int insert(Employee employee) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		namedParameterJdbcTemplate.update(INSERT, new BeanPropertySqlParameterSource(employee), keyHolder);
		logger.info("success insert employee id:{}", employee.getId());
		return keyHolder.getKey().intValue();
	}

	@CacheEvict(value = "empFindById", key = "#employeeId")
	public boolean delete(int employeeId) {
		if (namedParameterJdbcTemplate.update(DELETE, new MapSqlParameterSource("id", employeeId)) > 0) {
			logger.info("success delete employee id:{}", employeeId);
			return true;
		}
		return false;
	}

	@CachePut(value = "empFindById", key = "#employee.id")
	public Employee update(Employee employee) {
		namedParameterJdbcTemplate.update(UPDATE, new BeanPropertySqlParameterSource(employee));
		logger.info("employee:{}", employee);
		return this.findById(employee.getId());
	}

	@Cacheable(value = "empFindById", key = "#employeeId")
	public Employee findById(int employeeId) {
		try {
			return namedParameterJdbcTemplate.queryForObject(SELECT, new MapSqlParameterSource("id", employeeId), new EmployeeRowMapper());
		} catch (EmptyResultDataAccessException e) {
			logger.error("EmployeeDao findById failed, id: {}, Exception: {}", employeeId, e);
		}
		return null;
	}
	
	@Cacheable(value = "empFindById")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = SQLException.class)
	public int batchInsert(List<Employee> list) {
		SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(list.toArray());
		int[] updateCounts = namedParameterJdbcTemplate.batchUpdate(INSERT, batch);
		logger.info("employee size:{}", updateCounts.length);
		return updateCounts.length;
	}

}
