package com.tgl.security.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class ValueNotValidException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public ValueNotValidException(String value) {
		super("The value is not valid " + value);
	}
}
