package com.tgl.security.model;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Employee {

	private int id;

	@DecimalMax(value = "250", message = "height must be equal or less than 250")
	@DecimalMin(value = "100", message = "height must be equal or greater than 100")
	private int height;

	@DecimalMax(value = "200", message = "weight must be equal or less than 200")
	@DecimalMin(value = "30", message = "weight must be equal or greater than 30")
	private int weight;

	@Size(min = 4, max = 30, message = "Please provide a valid english name")
	private String englishName;

	@Size(min = 2, max = 4, message = "Please provide a valid chinese name")
	private String chineseName;

	@NotEmpty(message = "Please provide a phone number")
	@Pattern(regexp = "^[0-9]{4}", message = "Please provide a 4-digits phone number")
	private String phone;

	@Email(message = "Email must be a valid email address")
	@Size(max = 60, message = "Email address character should less than 60")
	private String email;
	
	private float bmi;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getChineseName() {
		return chineseName;
	}

	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public float getBmi() {
		return bmi;
	}

	public void setBmi(float bmi) {
		this.bmi = bmi;
	}

	@Override
	public String toString() {
		return "employee [height=" + height + ", weight=" + weight + ", englishName=" + englishName + ", chineseName="
				+ chineseName + ", phone=" + phone + ", email=" + email + ", id=" + id + ", bmi=" + getBmi() + "]\n";
	}

}
