package com.tgl.security.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.tgl.security.dao.EmployeeJdbcDao;
import com.tgl.security.model.Employee;
import com.tgl.security.util.DataUtil;

@Service
@Qualifier("empJdbcService")
public class EmployeeJdbcService {

	@Autowired
	private EmployeeJdbcDao employeeDao;

	public int insert(Employee employee) {
		employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight()));
		return employeeDao.insert(employee);
	}

	public boolean delete(int employeeId) {
		return employeeDao.delete(employeeId);
	}
	
	public Employee update(Employee employee) {
		employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight()));
		return employeeDao.update(employee);
	}

	public Employee findById(int employeeId) {
		Employee result = employeeDao.findById(employeeId);
		if (result == null) {
			return null;
		}
		String maskedName = DataUtil.maskChineseName(result.getChineseName());
		result.setChineseName(maskedName);
		return result;
	}
	
	public int batchInsert(List<Employee> list) {
		return employeeDao.batchInsert(list);
	}

}
