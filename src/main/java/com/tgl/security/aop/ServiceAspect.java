package com.tgl.security.aop;

import java.util.LinkedHashMap;
import java.util.Map;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.CodeSignature;
import org.slf4j.MDC;

@Aspect
public class ServiceAspect {
	
	@Pointcut("execution(* com.tgl.mybatis.service.*.*(..))")
	public void serviceMethods() {}
	
	@Around("serviceMethods()")
	public Object serviceAround(ProceedingJoinPoint joinPoint) throws Throwable {
		long start = System.currentTimeMillis();
		Object output = joinPoint.proceed();
		long elapsedTime = System.currentTimeMillis() - start;
		
		String className = joinPoint.getSignature().getDeclaringType() + "." + joinPoint.getSignature().getName();
		MDC.put("clasname", className);
		CodeSignature codeSignature = (CodeSignature) joinPoint.getSignature();
		Map<String, Object> params = new LinkedHashMap<>();
		for(int i = 0; i < codeSignature.getParameterNames().length; i++) {
			params.put(codeSignature.getParameterNames()[i], joinPoint.getArgs()[i]);
		}
		MDC.put("Execution time in milliseconds", String.valueOf(elapsedTime));
		return output;
	}
}
