package com.tgl.security.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	private static final Logger logger = LogManager.getLogger(SecurityConfig.class);
	
	@Value("${actuator.username}")
	private String username;

	@Value("${actuator.password}")
	private String password;

	@Value("${actuator.role}")
	private String role;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
		.inMemoryAuthentication()
		.withUser(username)
		.password(passwordEncoder().encode(password))
		.roles(role);
		
		logger.info("Welcome {}", username);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.authorizeRequests()
			.antMatchers("/swagger-ui.html/**")
			.authenticated() //存取必須通過驗證
		.and()
			.httpBasic()
		.and()
			.csrf().disable()
			.cors().disable();
		
		http
			.requestMatchers()
			.antMatchers("/swagger-ui.html/**", "/rest/**")
		.and()
			.authorizeRequests().anyRequest().hasRole(role);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
    }
}
